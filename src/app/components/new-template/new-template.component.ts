import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-template',
  templateUrl: './new-template.component.html',
  styleUrls: ['./new-template.component.scss']
})
export class NewTemplateComponent implements OnInit {

  verified:boolean=false;
  name:string='';
  newSectionTitle:string='';
  
  constructor() { }

  ngOnInit(): void {
  }

  verifyName(){
    this.verified=!this.verified;
  }
  getVerify(){
    if (!this.verified) {
      return 'basic'
    }
    return 'success'
  }

  editName(){
    this.verified=!this.verified;
  }


  addSection(){

  }
}
