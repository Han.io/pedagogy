import { Component, OnInit } from '@angular/core';
import { NbMenuItem, NbSidebarService, NbMenuService } from '@nebular/theme';
import { AppLoginService } from 'src/app/services/app-login.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit {

  constructor(private readonly sidebarService: NbSidebarService,
    private menuService: NbMenuService, public loginService: AppLoginService) { }

  ngOnInit(): void {
    this.menuService.onItemClick().subscribe((event)=>{
      if (event.item.title==='Log out') {

        console.log('logout clicked');
        this.loginService.logOut();
      }
    }

    );
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true);
    return false;
  }

  

  items: NbMenuItem[] = [
    {
      title: 'Home',
      icon: 'home-outline',
      link: '/home'
    },
    {
      title: 'Templates',
      icon: 'file-outline',
      link: '/templates'
    },
    {
      title: 'Reports',
      icon: 'file-text-outline',
      link: '/reports'
      /* icon: { icon: 'checkmark-outline', pack: 'eva' }, */
    },
    {
      title: 'Students',
      icon: 'person-outline',
      link: '/students'
    },
    {
      title: 'Log out',
      icon: 'corner-down-left-outline',
    },
  ];
}
