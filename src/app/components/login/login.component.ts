import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppLoginService } from 'src/app/services/app-login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  

  constructor(private rutas:Router, public loginService:AppLoginService) { }

  ngOnInit(): void {
    
  }

  login() {
    this.loginService.logIn();
    /* console.log('login!');
    this.rutas.navigate(['/home']); */
  }


}
