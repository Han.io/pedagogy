import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { from } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AppLoginService {

  user!: SocialUser | undefined;
  loggedIn!: boolean;

  constructor(private authService: SocialAuthService, private router: Router) { 
    this.loggedIn = false;
  }

  googleLogIn() {
    return from(this.authService.signIn(GoogleLoginProvider.PROVIDER_ID))
  }

  logIn() {
    this.googleLogIn().subscribe(user => {
      this.user = user;
      
      if (this.user.email.includes('@ccs.edu.bo')) {
        this.loggedIn = true;
        console.log(`LogIn this.user: ${JSON.stringify(this.user)}`);
        this.router.navigate(['/home']);
      }else{
        this.logOut();
      }
    })
  }

  logOut() {
    this.authService.signOut().then(user => {
      console.log(`LogOut this.user: ${JSON.stringify(this.user)}`);
      this.user = undefined;
      this.loggedIn = false;
      this.router.navigate(['/']);
    });
  }
}
