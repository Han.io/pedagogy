import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//Google login
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';

//Nebular and MaterialModules
import {  NbThemeModule, 
          NbLayoutModule,
          NbMenuModule,
          NbSidebarModule,
          NbIconModule,
          NbInputModule,
          NbButtonModule,
          NbCardModule,
          NbFormFieldModule,
          NbActionsModule,
          NbTooltipModule,
          NbPopoverModule,
          NbSelectModule,
          NbUserModule,
         } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import { MatSliderModule } from '@angular/material/slider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//Components
import { LoginComponent } from './components/login/login.component';
import { MainNavComponent } from './components/navigation/main-nav/main-nav.component';
import { HomeComponent } from './components/home/home.component';
import { TemplatesComponent } from './components/templates/templates.component';
import { ReportsComponent } from './components/reports/reports.component';
import { StudentsComponent } from './components/students/students.component';
import { NewTemplateComponent } from './components/new-template/new-template.component';
import { NewStudentComponent } from './components/new-student/new-student.component';
import { NewReportComponent } from './components/new-report/new-report.component';
import { ViewReportComponent } from './components/view-report/view-report.component';
import { ViewStudentComponent } from './components/view-student/view-student.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainNavComponent,
    HomeComponent,
    TemplatesComponent,
    ReportsComponent,
    StudentsComponent,
    NewTemplateComponent,
    NewStudentComponent,
    NewReportComponent,
    ViewReportComponent,
    ViewStudentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SocialLoginModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbIconModule,
    NbLayoutModule,
    NbInputModule,
    NbButtonModule,
    NbCardModule,
    NbEvaIconsModule,
    MatSliderModule,
    NgbModule,
    NbFormFieldModule,
    FormsModule,
    NbActionsModule,
    NbTooltipModule,
    NbPopoverModule,
    NbSelectModule,
    NbUserModule,
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '838329151906-9ulj8lekn0go9b4gcvgr00ik7t4mssa8.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
