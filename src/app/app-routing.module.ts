import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { MainNavComponent } from './components/navigation/main-nav/main-nav.component';
import { HomeComponent } from './components/home/home.component';
import { TemplatesComponent } from './components/templates/templates.component';
import { ReportsComponent } from './components/reports/reports.component';
import { StudentsComponent } from './components/students/students.component';
import { NewTemplateComponent } from './components/new-template/new-template.component';
import { NewReportComponent} from './components/new-report/new-report.component';
import {NewStudentComponent} from './components/new-student/new-student.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'home', component: MainNavComponent,
    children: [{path: '', component: HomeComponent},]
  },
  {path: 'templates', component: MainNavComponent,
    children: [{path: '', component: TemplatesComponent},]
  },
  {path: 'reports', component: MainNavComponent,
    children: [{path: '', component: ReportsComponent},]
  },
  {path: 'students', component: MainNavComponent,
    children: [{path: '', component: StudentsComponent},]
  },
  {path: 'templates/new-template', component: MainNavComponent,
    children: [{path: '', component: NewTemplateComponent},]
  },
  {path: 'reports/new-report', component: MainNavComponent,
    children: [{path: '', component: NewReportComponent},]
  },
  {path: 'students/new-student', component: MainNavComponent,
    children: [{path: '', component: NewStudentComponent},]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
